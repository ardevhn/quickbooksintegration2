using System;

namespace QuickbooksIntegration.Web.Api.Infrastructure.RestExceptions
{
    public class TokenDoesNotExistException : Exception
    {
    }
}