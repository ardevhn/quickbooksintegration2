﻿namespace QuickbooksIntegration.Web.Api.Infrastructure.Authentication.Roles
{
    public class Feature
    {
         public string Description { get; set; }
    }
}