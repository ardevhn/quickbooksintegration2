using System;
using QuickbooksIntegration.Domain.Entities;
using Autofac;
using AutoMapper;
using QuickbooksIntegration.Web.Api.Requests;
using QuickbooksIntegration.Web.Api.Responses.Admin;

namespace QuickbooksIntegration.Web.Api.Infrastructure.Configuration
{
    public class ConfigureAutomapperMappings : IBootstrapperTask<ContainerBuilder>
    {
        #region IBootstrapperTask<ContainerBuilder> Members

        public Action<ContainerBuilder> Task
        {
            get
            {
                return container =>
                    {
                        Mapper.CreateMap<User, AdminUserResponse>();
                        Mapper.CreateMap<UserAbility, UserAbilityRequest>().ReverseMap();
                    };
            }
        }

        #endregion
    }
}