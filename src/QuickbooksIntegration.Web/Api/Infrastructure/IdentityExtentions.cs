using AcklenAvenue.Commands;
using QuickbooksIntegration.Domain.Entities;
using Nancy;
using QuickbooksIntegration.Web.Api.Infrastructure.Authentication;
using QuickbooksIntegration.Web.Api.Infrastructure.Exceptions;

namespace QuickbooksIntegration.Web.Api.Infrastructure
{
    public static class IdentityExtentions
    {
        public static IUserSession UserSession(this NancyModule module)
        {
            var user = module.Context.CurrentUser as LoggedInUserIdentity;
            if (user == null) throw new NoCurrentUserException();
            return user.UserSession;
        }

        public static UserLoginSession UserLoginSession(this NancyModule module)
        {
            var user = module.Context.CurrentUser as LoggedInUserIdentity;
            if (user == null || user.UserSession.GetType() != typeof(UserLoginSession)) throw new NoCurrentUserException();
            return (UserLoginSession) user.UserSession;
        }
    }
}