﻿namespace QuickbooksIntegration.Web.Api
{
    public interface IBaseUrlProvider
    {
        string GetBaseUrl();
    }
}