namespace QuickbooksIntegration.Web.Api.Requests
{
    public class ResetPasswordRequest
    {
        public string Email { get; set; }
    }
}