﻿angular.module("Application.Services").factory("adminService", function ($httpq) {

    return {
        initQuickBooks: function(email, host) {
            return $httpq.get("/qbInit/" + email + "/" + host );
        },
        saveQuickbookToken: function(payload) {
            return $httpq.post("/saveQuickbookToken", payload);
        },
        isQuickbookAssociated: function(userEmail) {
            return $httpq.get("/isQuickbookAssociated/" + userEmail);
        },
        GetUsers: function (payload) {
            return $httpq.get("/users?" + "PageNumber=" + payload.PageNumber + "&PageSize=" + payload.PageSize + "&Field=" + payload.Field);
        },
        EnableUser: function (payload) {
            return $httpq.post("/users/enable", payload);
        },
        GetUser: function(id) {
            return $httpq.get("/user/" + id );
        },
        UpdateProfile: function(payload) {
            return $httpq.post("/user/", payload);
        },
        GetRol:function() {
            return $httpq.get("/rol");
        }
        
    };
});