﻿angular.module("Application.Controllers").controller("syncController",
    function ($scope, userService, $location, homeService, toastr, Upload, blockUI, $q, $route, $document) {
        var user = userService.GetUser();

        angular.element(document).ready(function () {
             
            var criticalTime = 180;
            var nonCriticalTime = 300;
            var initCriticalHour = 8;
            var endCriticalHour = 17;
             
            var setCountDownValue = function () {
                    var currentHour = new Date().getHours();
                    if (currentHour >= initCriticalHour && currentHour < endCriticalHour)
                        $scope.countdownvalue = criticalTime;
                    else
                        $scope.countdownvalue = nonCriticalTime;
                };
                         
            setCountDownValue();
        });

        $scope.tabSelected = 1;
        if (!user) {
            $location.path("/login");
        }
        $scope.$parent.title = "Sync";
        $scope.importInvoices = function () {
            $("#excelFile").click();
        };
        $scope.$watch("excelFileData", function (newValue) {

            if (newValue && newValue[0]) {
                if ($scope.excelFileData) {
                    var file = $scope.excelFileData[0];
                    blockUI.start("Importing Invoices");
                    Upload.upload({
                        url: "/insertInvoices",
                        method: "POST",
                        fields: { 'userEmail': user.email },
                        file: file
                    }).success(function () {
                        homeService.pendingInvoices(user.email).then(function (invoiceData) {
                            $scope.invoices = invoiceData;
                            toastr.success("Invoices imported successfully.");
                            blockUI.stop();
                        });
                    }).error(function () {
                        toastr.error("There was an error trying to import the invoices");
                        blockUI.stop();
                    });
                }

            }
        });
        var buildPayload = function (invoice) {
            return {
                UserEmail: user.email,
                BrokerName: invoice.brokerName,
                LoadNumber: invoice.loadNumber,
                BillingAddress: invoice.billingAddress,
                City: invoice.city,
                Country: invoice.country,
                DueDate: invoice.dueByDate,
                InvoiceDate: invoice.invoiceDate,
                BasicFreightRate: invoice.basicFreightRate,
                ExtraPickUp: invoice.extraPickUp,
                ExtraDelivery: invoice.extraDelivery,
                WaitingTime: invoice.waitingTime,
                LumperFees: invoice.lumperFees,
                Overnight: invoice.overnight,
                Others: invoice.others,
                FuelSurcharge: invoice.fuelSurcharge,
                CongestionFee: invoice.congestionFee,
                BondFee: invoice.bondFee,
                BrokerAdvance: invoice.brokerAdvance,
                BrokerAdvanceFee: invoice.brokerAdvanceFee,
                Discount: invoice.discount,
                TotalAmount: invoice.totalAmount,
                CustomerReferenceNumber: invoice.customerReferenceNumber,
                PickUpDate: invoice.pickUpDate,
                PickUpCity: invoice.pickUpCity,
                DeliveryCity: invoice.deliveryCity,
                SOSInvoiceNumber: invoice.sosInvoiceNumber,
                BorderFee: invoice.borderFee ? invoice.borderFee : 0,
                Hazardous: invoice.hazardous ? invoice.hazardous : 0
            }
        }

        $scope.startTimer = function () {
            $scope.$broadcast("timer-start");
            $scope.ableToSearch = true;
        };

        $scope.resetClock = function () {
            $scope.$broadcast("timer-reset");
        }

        $scope.finished = function () {
            $scope.$broadcast("timer-reset");
            $scope.$broadcast("timer-resume");
            $scope.$apply();
            $scope.checkFtpAndSync();
        };

        $scope.i = 0;
        $scope.ii = 0;

        $scope.checkFtpAndSync = function () {
            var promiseArray = [];
            var externalPromiseArray = [];

            blockUI.start("Checking Ftp");
            homeService.checkFtpInvoice(user.email).then(function () {
                homeService.pendingInvoices(user.email).then(function (invoiceData) {
                    $scope.i = 1;
                    $scope.ii = 0;
                    invoiceData.forEach(function (invoice) {
                        var defered = $q.defer();
                        var innerDefer = $q.defer();
                        $q.all(promiseArray).then(function () {
                            var payload = buildPayload(invoice);
                            blockUI.start("syncing Invoice #" + invoice.sosInvoiceNumber + " " + $scope.i + " of " + invoiceData.length);
                            homeService.createInvoice(payload).then(function () {
                                $scope.i = $scope.i + 1;
                                $scope.ii = $scope.ii + 1;
                                homeService.syncedInvoices(user.email).then(function (syncedInvoice) {
                                    blockUI.stop();
                                    blockUI.stop();
                                    $scope.invoices = syncedInvoice;
                                    defered.resolve();
                                    innerDefer.resolve();
                                }, function () {
                                    blockUI.stop();
                                    toastr.error("There were some errors trying to sync the invoices, Please try again");
                                    defered.reject();
                                    innerDefer.reject();
                                });
                            }, function () {
                                blockUI.stop();
                                toastr.error("There were some errors trying to sync the invoices, Please try again");
                                defered.reject();
                                innerDefer.reject();
                            });
                        });
                        promiseArray.push(defered.promise);
                        externalPromiseArray.push(innerDefer.promise);
                    });
                    if (invoiceData.length === 0) {
                        blockUI.stop();
                    }
                });
            });
        }
        homeService.syncedInvoices(user.email).then(function (invoices) {
            $scope.invoices = invoices;
            $scope.startTimer();
        });
    });
