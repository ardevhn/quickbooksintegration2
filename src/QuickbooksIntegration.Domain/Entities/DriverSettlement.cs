﻿using System;

namespace QuickbooksIntegration.Domain.Entities
{
    public class DriverSettlement : Entity
    {
        public DriverSettlement()
        {
            Id = Guid.NewGuid();
        }

        public virtual User User { get; set; }
        public virtual string QbId { get; set; }

        public virtual string TruckOwner { get; set; }
        public virtual string Driver { get; set; }
        public virtual string TruckNumber { get; set; }
        public virtual string FileNumber { get; set; }
        public virtual string OoPay { get; set; }
        public virtual string BasicDriverPay { get; set; }
        public virtual string WaitingTime { get; set; }
        public virtual string ExtraPickUp { get; set; }
        public virtual string ExtraDelivery { get; set; }
        public virtual string Overnight { get; set; }
        public virtual string Others { get; set; }
        public virtual string EmptyMileDebit { get; set; }
        public virtual string ExtraMiles { get; set; }
        public virtual string FuelSurcharge { get; set; }
        public virtual string SubTotal1 { get; set; }
        public virtual string Fuel { get; set; }
        public virtual string DriverCash { get; set; }
        public virtual string FuelFee { get; set; }
        public virtual string RefFuel { get; set; }
        public virtual string FuelRebate { get; set; }
        public virtual string SubTotal2 { get; set; }
        public virtual string EfsCashAdvances { get; set; }
        public virtual string EfsFee { get; set; }
        public virtual string WesternUnion { get; set; }
        public virtual string OwnChecks { get; set; }
        public virtual string Transcheck { get; set; }
        public virtual string AdvanceFee { get; set; }
        public virtual string Loan { get; set; }
        public virtual string LoanFee { get; set; }
        public virtual string NegativeFile { get; set; }
        public virtual string QuickPayFee { get; set; }
        public virtual string QuickPayPercent { get; set; }
        public virtual string SubTotal3 { get; set; }
        public virtual string TruckLease { get; set; }
        public virtual string LicensePlate { get; set; }
        public virtual string RoadTax { get; set; }
        public virtual string TrailerLease { get; set; }
        public virtual string TrailerRent { get; set; }
        public virtual string SubTotal4 { get; set; }
        public virtual string LumperFees { get; set; }
        public virtual string ReDiesel { get; set; }
        public virtual string ReWash { get; set; }
        public virtual string ReTolls { get; set; }
        public virtual string ReRepairs { get; set; }
        public virtual string ReMaintenance { get; set; }
        public virtual string ReHotel { get; set; }
        public virtual string ReWTickets { get; set; }
        public virtual string ReOil { get; set; }
        public virtual string ReTires { get; set; }
        public virtual string ReBonus { get; set; }
        public virtual string ReOwnRepairs { get; set; }
        public virtual string ReOwnMaintenance { get; set; }
        public virtual string ReParking { get; set; }
        public virtual string ReIdleAir { get; set; }
        public virtual string ReFees { get; set; }
        public virtual string ReOthers { get; set; }
        public virtual string SubTotal5 { get; set; }
        public virtual string AccidentalInsurance { get; set; }
        public virtual string OoPackage { get; set; }
        public virtual string ChildSupport { get; set; }
        public virtual string Insurance { get; set; }
        public virtual string Yard { get; set; }
        public virtual string DotComplaince { get; set; }
        public virtual string Escrow { get; set; }
        public virtual string CoDriverPay { get; set; }
        public virtual string OoAdvance { get; set; }
        public virtual string OoAdvanceCoDriver { get; set; }
        public virtual string OdOthers { get; set; }
        public virtual string SubTotal6 { get; set; }
        public virtual string Signs { get; set; }
        public virtual string Simplex { get; set; }
        public virtual string FinePenalty { get; set; }
        public virtual string CheckAmount { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Zip { get; set; }
        public virtual string Country { get; set; }
        public virtual string DriverType { get; set; }
        public virtual string TransactionType { get; set; }
        public virtual string Maintenance { get; set; }

        public virtual bool MarkAsUpdate { get; set; }
        public virtual string SyncDate { get; set; }
        public virtual string Status { get; set; }
    }
}
