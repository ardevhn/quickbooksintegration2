using System;

namespace QuickbooksIntegration.Domain.Services
{
    public interface ITimeProvider
    {
        DateTime Now();
    }
}